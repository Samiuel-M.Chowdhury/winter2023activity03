
public class Application {

	public static void main(String[] args) {
		Student Sam=new Student();
		Sam.name="Sam";
		Sam.age=17;
		Sam.programType="Technical";
		
		System.out.println(Sam.name);
		System.out.println(Sam.age);
		System.out.println(Sam.programType);
		
		Student Chow=new Student();
		Chow.name="Poly";
		Chow.age=20;
		Chow.programType="Pre-University";
		
		System.out.println(Chow.name);
		System.out.println(Chow.age);
		System.out.println(Chow.programType);
		
		Sam.predictGraduation(Sam.age, Sam.programType);
		Chow.describStudentMajor(Chow.name, Chow.programType);
		
		Student[] section3=new Student[3];
		section3[0]=Sam;
		section3[1]=Chow;
		System.out.println(section3[0].name);
		
		section3[2] = new Student();
		section3[2] =Sam ;
		System.out.println(section3[2].programType);
		

	}

}

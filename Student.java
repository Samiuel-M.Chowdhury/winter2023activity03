
public class Student {

	public String programType;
	public String name;
	public int age;
	
	public void predictGraduation(int age, String programType) {
		int yearsOfStudying;
		if(programType.equals("Technical")) {
			yearsOfStudying=3;
		}else {
			yearsOfStudying=2;
		}
		System.out.println("You will graduate by the age of "+(yearsOfStudying+age));
	}
	
	public void describStudentMajor(String name, String programType) {
		System.out.println(name +" majors in a "+ programType+" program.");
	}

}
